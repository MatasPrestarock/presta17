<?php
/**
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

declare(strict_types=1);

if (!defined('_PS_VERSION_')) {
    exit;
}

class Msonboard extends Module
{
    /**
     * @var string
     */
    private $html = '';

    public function __construct()
    {
        $this->name = 'msonboard';
        $this->tab = 'front_office_features';
        $this->version = '1.0';
        $this->author = 'Prestarock';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.7');
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Onboard module');
        $this->description = $this->l('Description of my onboard module');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    /**
     * @throws PrestaShopException
     */
    public function install(): bool
    {
        if (Shop::isFeatureActive()) Shop::setContext(Shop::CONTEXT_ALL);

        if (!parent::install() || !Configuration::updateValue('MSONBOARD_ENTRY', 'some value')) return false;

        return true;
    }

    /**
     * @return bool
     */
    public function uninstall(): bool
    {
        if(!parent::uninstall() || !Configuration::deleteByName('MSONBOARD_ENTRY')) return false;

        return true;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        $this->postProcess();
        return $this->html.$this->displayForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submit'.$this->name)) {
            $name = strval(Tools::getValue('MSONBOARD_ENTRY'));
            if (!$name || empty($name) || !Validate::isGenericName($name)) {
                $this->html .= $this->displayError($this->l('Invalid configuration value'));
            } else {
                Configuration::updateValue('MSONBOARD_ENTRY', $name);
                $this->html .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }
    }

    /**
     * @return string
     */
    public function displayForm(): string
    {
        $defaultLang = (int)Configuration::get('PS_LANG_DEFAULT');

        $fieldsForm[0]['form'] = [
            'legend' => [
                'title' => $this->l('Settings'),
            ],
            'input' => [
                [
                    'type'  => 'text',
                    'label' => $this->l('Configuration value'),
                    'name'  => 'MSONBOARD_ENTRY',
                    'size'  => 20,
                    'required'  => true,
                ]
            ],
            'submit' => [
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ],
        ];

        $helper = new HelperForm();

        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $defaultLang;
        $helper->allow_employee_form_lang = $defaultLang;
        $helper->title = $this->displayName;
        $helper->show_toolbar =  true;
        $helper->toolbar_scroll = true;
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = [
            'save' => [
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                    '&token'.Tools::getAdminTokenLite('AdminModules'),
            ],
            'back' => [
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back')
            ]
        ];

        $helper->fields_value['MSONBOARD_ENTRY'] = Configuration::get('MSONBOARD_ENTRY');

        return $helper->generateForm($fieldsForm);
    }
}
