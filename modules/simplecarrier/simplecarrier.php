<?php

declare(strict_types=1);

use PrestaRock\SimpleCarrier\Install\Installer;
use Symfony\Component\Config\ConfigCache;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Dumper\PhpDumper;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

if (!defined('_PS_VERSION_')) {
    exit;
}

class SimpleCarrier extends CarrierModule
{

    const DISABLE_CACHE = true;

    /**
     * Symfony DI container
     * @var
     */
    private $moduleContainer;

    public function __construct()
    {
        $this->name = 'simplecarrier';
        $this->tab = 'shipping_logistics';
        $this->version = '1.0.0';
        $this->ps_versions_compliancy = ['min' => '1.7', 'max' => _PS_VERSION_];
        $this->author = 'PrestaRock';
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('SimpleCarrier module');
        $this->description = $this->l('Description of SimpleCarrier module');
        $this->autoLoad(); // clasiu autolaudas reikalingas symfoni DI
        $this->compile(); // Symfony DI
    }

    public function install(): bool
    {
        if (!parent::install()) {
            return false;
        }

        $installer = $this->getModuleContainer()->get(Installer::class);
        if (!$installer->install()) {
            $this->_errors .= $installer->getErrors();
            return false;
        }

        return true;
    }

    public function getOrderShippingCost($params, $shipping_cost)
    {
        // TODO: Implement getOrderShippingCost() method.
    }

    public function getOrderShippingCostExternal($params)
    {
        // TODO: Implement getOrderShippingCostExternal() method.
    }

    public function getModuleContainer($id = false)
    {
        if ($id) {
            return $this->moduleContainer->get($id);
        }

        return $this->moduleContainer;
    }

    /**
     * @throws Exception
     */
    private function compile()
    {
        $containerCache = $this->getLocalPath() . 'var/cache/container.php';
        $containerConfigCache = new ConfigCache($containerCache, self::DISABLE_CACHE);
        $containerClass = get_class($this) . 'Container';
        if (!$containerConfigCache->isFresh()) {
            $containerBuilder = new ContainerBuilder();
            $locator = new FileLocator($this->getLocalPath() . 'config');
            $loader = new YamlFileLoader($containerBuilder, $locator);
            $loader->load('config.yml');
            $containerBuilder->compile();
            $dumper = new PhpDumper($containerBuilder);
            $containerConfigCache->write(
                $dumper->dump(['class' => $containerClass]),
                $containerBuilder->getResources()
            );
        }
        require_once $containerCache;
        $this->moduleContainer = new $containerClass();
    }

    private function autoLoad()
    {
        require_once $this->getLocalPath() . 'vendor/autoload.php';
    }


}