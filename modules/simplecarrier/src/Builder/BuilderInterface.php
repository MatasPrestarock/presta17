<?php


namespace PrestaRock\SimpleCarrier\Builder;


interface BuilderInterface
{

    /**
     * save result
     * @return mixed
     */
    public function save();

    /**
     * return all errors if any
     * @return mixed
     */
    public function getErrors();

}