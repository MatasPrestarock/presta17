<?php

namespace PrestaRock\SimpleCarrier\Builder;

use Carrier;
use PrestaShopException;
use Shop;
use SimpleCarrier;

class CarrierBuilder implements BuilderInterface
{

    private $module;

    private $active;

    private $shippingMethod;

    /**
     * @var array
     */
    private $delay;

    private $name;

    private $shippingExternal;

    /**
     * @var array
     */
    private $errors;

    private $idCarrier;

    private $idShop;

    private $zones;

    private $groups;

    public function __construct(SimpleCarrier $module)
    {
        $this->module = $module;
    }

    public function save(): bool
    {
        $carrier = new Carrier(); // instantiate new Carrier object model
        $carrier->active = $this->active; // set if active
        $carrier->shipping_method = $this->shippingMethod; // set shipping method
        $carrier->delay = $this->delay; // set delay text
        $carrier->name = $this->name; // set carrier name
        $carrier->shipping_external = $this->shippingExternal; // set if external
        $carrier->external_module_name = $this->module->name; // set external module name
        $carrier->url = '';
        $carrier->id_shop_list = Shop::getShops(true, null, true); // list of shop id's

        $validationError = $carrier->validateFields(false, true); // get error massage if error
        $langValidationError = $carrier->validateFieldsLang(false, true); // get error massage if error

        if (true !== $validationError) {
            $this->errors[] = $validationError; // add them for shop admin to see
        }

        if (true !== $langValidationError) {
            $this->errors[] = $langValidationError; // add them for shop admin to see
        }

        if ($this->errors) {
            return false;
        }

        try {
            $carrier->save(); // attempt to create carrier
        } catch (PrestaShopException $e) {
            $this->errors[] = sprintf(
                $this->module->l('Unable to create carrier'),
                $carrier->name
            );

            return false;
        }

        if (!$this->idCarrier) {
            foreach ($this->zones as $zone) {
                $carrier->addZone($zone['id_zone']);
            }
            //$carrier->insertDeliveryPlaceholder($this->zones);
            $carrier->setGroups($this->groups);
        }

        // multishop
        if (!$this->idShop) {
            $shopIds = Shop::getContextListShopID();
            $carrier->associateTo($shopIds); // multiple shops
        } else {
            $carrier->associateTo($this->idShop); // single shop
        }

        return true;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function setIdCarrier($idCarrier)
    {
        $this->idCarrier = $idCarrier;
    }

    public function setActive(int $int)
    {
        $this->active = $int;
    }

    public function setShippingMethod(int $shippingMethod)
    {
        $this->shippingMethod = $shippingMethod;
    }

    public function setDelay(array $delay)
    {
        $this->delay = $delay;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setShippingExternal(int $shippingExternal)
    {
        $this->shippingExternal = $shippingExternal;
    }

    public function setZones(array $psZones)
    {
        $this->zones = $psZones;
    }

    public function setGroups(array $psGroupIds)
    {
        $this->groups = $psGroupIds;
    }

    public function setIdShop(int $idShop)
    {
        $this->idShop = $idShop;
    }

}