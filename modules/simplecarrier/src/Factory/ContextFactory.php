<?php


namespace PrestaRock\SimpleCarrier\Factory;

use Context;

class ContextFactory
{

    public function getLanguage()
    {
        return Context::getContext()->language;
    }

}