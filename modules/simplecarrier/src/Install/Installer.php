<?php

/* PrestaRock */

declare(strict_types=1);

namespace PrestaRock\SimpleCarrier\Install;

use SimpleCarrier;
use PrestaRock\SimpleCarrier\Service\CreateCarrierService;

class Installer
{

    /**
     * @var SimpleCarrier
     */
    private $module;

    /**
     * @var CreateCarrierService
     */
    private $createCarrierService;

    public function __construct(SimpleCarrier $module, CreateCarrierService $createCarrierService)
    {
        $this->module = $module;
        $this->createCarrierService = $createCarrierService;
    }

    /**
     * Attempt to install SimpleCarrier module
     * @return bool
     */
    public function install(): bool
    {
        if (!$this->registerHooks()) {
            return false;
        }

        if (!$this->createCarrier()) {
            return false;
        }

        return true;
    }

    public function getErrors(): ?array
    {
        return $this->createCarrierService->getErrors();
    }

    /**
     * Register module hooks
     * @return bool
     */
    private function registerHooks(): bool
    {
        $hooks = $this->getHooks();

        if (empty($hooks)) {
            return true;
        }

        return $this->module->registerHook($hooks);
    }

    /**
     * List of hooks
     * @return string[]
     */
    private function getHooks(): array
    {
        return [
            'displayCarrierExtraContent', // Carrier info in FO for ex. pickup point select etc.
            'actionCarrierProcess', // This hook is fired BEFORE the new OrderStatus is saved into the database
            'updateCarrier', // Hook to manage carrier ID
        ];
    }

    private function createCarrier(): bool
    {
        return $this->createCarrierService->createCarrier();
    }

}