<?php


namespace PrestaRock\SimpleCarrier\Service;

use Carrier;
use Group;
use Language;
use Validate;
use Zone;
use PrestaRock\SimpleCarrier\Builder\CarrierBuilder;

class CreateCarrierService
{

    /**
     * @var Language
     */
    private $language;

    /**
     * @var CarrierBuilder
     */
    private $carrierBuilder;

    private $errors = [];

    public function __construct(Language $language, CarrierBuilder $carrierBuilder)
    {
        $this->language = $language;
        $this->carrierBuilder = $carrierBuilder;
    }

    public function createCarrier(): bool
    {
        $psZones = Zone::getZones();
        $psGroups = Group::getGroups($this->language->id);
        $psGroupIds = [];

        foreach ($psGroups as $psGroup) {
            $psGroupIds[] = $psGroup['id_group'];
        }

        $carrierBuilder = $this->carrierBuilder;
        $idCarrier = null;
        $carrierBuilder->setIdCarrier($idCarrier);
        $carrierBuilder->setActive(0);
        $carrierBuilder->setShippingMethod(Carrier::SHIPPING_METHOD_WEIGHT);
        $carrierBuilder->setDelay($this->fillCarrierMultilangField('2 darbo dienos'));
        $carrierBuilder->setName('SimpleCarrier carrier');
        $carrierBuilder->setShippingExternal(1);
        $carrierBuilder->setZones($psZones);
        $carrierBuilder->setGroups($psGroupIds);
        $this->errors = $carrierBuilder->getErrors(); // get errors if any
        return $carrierBuilder->save();
    }

    public function getErrors(): ?array
    {
        return $this->errors;
    }

    private function fillCarrierMultilangField($delay)
    {
        $values = [];
        foreach (Language::getLanguages() as $language) {
            $values[$language['id_lang']] = $delay;
        }

        return $values;
    }

}