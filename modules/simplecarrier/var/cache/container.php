<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;

/**
 * This class has been auto-generated
 * by the Symfony Dependency Injection Component.
 *
 * @final since Symfony 3.3
 */
class SimpleCarrierContainer extends Container
{
    private $parameters = [];
    private $targetDirs = [];

    public function __construct()
    {
        $this->services = [];
        $this->normalizedIds = [
            'prestarock\\simplecarrier\\builder\\carrierbuilder' => 'PrestaRock\\SimpleCarrier\\Builder\\CarrierBuilder',
            'prestarock\\simplecarrier\\install\\installer' => 'PrestaRock\\SimpleCarrier\\Install\\Installer',
            'prestarock\\simplecarrier\\service\\createcarrierservice' => 'Prestarock\\SimpleCarrier\\Service\\CreateCarrierService',
            'prestarock\\simplecarrier\\service\\updatecarrierservice' => 'Prestarock\\SimpleCarrier\\Service\\UpdateCarrierService',
        ];
        $this->methodMap = [
            'PrestaRock\\SimpleCarrier\\Builder\\CarrierBuilder' => 'getCarrierBuilderService',
            'PrestaRock\\SimpleCarrier\\Install\\Installer' => 'getInstallerService',
            'Prestarock\\SimpleCarrier\\Service\\CreateCarrierService' => 'getCreateCarrierServiceService',
            'Prestarock\\SimpleCarrier\\Service\\UpdateCarrierService' => 'getUpdateCarrierServiceService',
            'language' => 'getLanguageService',
            'simplecarrier' => 'getSimplecarrierService',
        ];
        $this->privates = [
            'PrestaRock\\SimpleCarrier\\Builder\\CarrierBuilder' => true,
            'Prestarock\\SimpleCarrier\\Service\\CreateCarrierService' => true,
            'Prestarock\\SimpleCarrier\\Service\\UpdateCarrierService' => true,
            'language' => true,
            'simplecarrier' => true,
        ];

        $this->aliases = [];
    }

    public function getRemovedIds()
    {
        return [
            'PrestaRock\\SimpleCarrier\\Builder\\CarrierBuilder' => true,
            'Prestarock\\SimpleCarrier\\Service\\CreateCarrierService' => true,
            'Prestarock\\SimpleCarrier\\Service\\UpdateCarrierService' => true,
            'Psr\\Container\\ContainerInterface' => true,
            'Symfony\\Component\\DependencyInjection\\ContainerInterface' => true,
            'language' => true,
            'simplecarrier' => true,
        ];
    }

    public function compile()
    {
        throw new LogicException('You cannot compile a dumped container that was already compiled.');
    }

    public function isCompiled()
    {
        return true;
    }

    public function isFrozen()
    {
        @trigger_error(sprintf('The %s() method is deprecated since Symfony 3.3 and will be removed in 4.0. Use the isCompiled() method instead.', __METHOD__), E_USER_DEPRECATED);

        return true;
    }

    /**
     * Gets the public 'PrestaRock\SimpleCarrier\Install\Installer' shared service.
     *
     * @return \PrestaRock\SimpleCarrier\Install\Installer
     */
    protected function getInstallerService()
    {
        return $this->services['PrestaRock\\SimpleCarrier\\Install\\Installer'] = new \PrestaRock\SimpleCarrier\Install\Installer(${($_ = isset($this->services['simplecarrier']) ? $this->services['simplecarrier'] : $this->getSimplecarrierService()) && false ?: '_'}, ${($_ = isset($this->services['Prestarock\\SimpleCarrier\\Service\\CreateCarrierService']) ? $this->services['Prestarock\\SimpleCarrier\\Service\\CreateCarrierService'] : $this->getCreateCarrierServiceService()) && false ?: '_'});
    }

    /**
     * Gets the private 'PrestaRock\SimpleCarrier\Builder\CarrierBuilder' shared service.
     *
     * @return \PrestaRock\SimpleCarrier\Builder\CarrierBuilder
     */
    protected function getCarrierBuilderService()
    {
        return $this->services['PrestaRock\\SimpleCarrier\\Builder\\CarrierBuilder'] = new \PrestaRock\SimpleCarrier\Builder\CarrierBuilder(${($_ = isset($this->services['simplecarrier']) ? $this->services['simplecarrier'] : $this->getSimplecarrierService()) && false ?: '_'});
    }

    /**
     * Gets the private 'Prestarock\SimpleCarrier\Service\CreateCarrierService' shared service.
     *
     * @return \PrestaRock\SimpleCarrier\Service\CreateCarrierService
     */
    protected function getCreateCarrierServiceService()
    {
        return $this->services['Prestarock\\SimpleCarrier\\Service\\CreateCarrierService'] = new \PrestaRock\SimpleCarrier\Service\CreateCarrierService(${($_ = isset($this->services['language']) ? $this->services['language'] : $this->getLanguageService()) && false ?: '_'}, ${($_ = isset($this->services['PrestaRock\\SimpleCarrier\\Builder\\CarrierBuilder']) ? $this->services['PrestaRock\\SimpleCarrier\\Builder\\CarrierBuilder'] : $this->getCarrierBuilderService()) && false ?: '_'});
    }

    /**
     * Gets the private 'Prestarock\SimpleCarrier\Service\UpdateCarrierService' shared service.
     *
     * @return \PrestaRock\SimpleCarrier\Service\UpdateCarrierService
     */
    protected function getUpdateCarrierServiceService()
    {
        return $this->services['Prestarock\\SimpleCarrier\\Service\\UpdateCarrierService'] = new \PrestaRock\SimpleCarrier\Service\UpdateCarrierService();
    }

    /**
     * Gets the private 'language' shared service.
     *
     * @return \Language
     */
    protected function getLanguageService()
    {
        return $this->services['language'] = \PrestaRock\SimpleCarrier\Factory\ContextFactory::getLanguage();
    }

    /**
     * Gets the private 'simplecarrier' shared service.
     *
     * @return \SimpleCarrier
     */
    protected function getSimplecarrierService()
    {
        return $this->services['simplecarrier'] = \Module::getInstanceByName('simplecarrier');
    }
}
