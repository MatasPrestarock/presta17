<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'a0fbac5b22ca489527aae14bd20032d899edc193',
        'name' => 'prestarock/simplecarrier',
        'dev' => true,
    ),
    'versions' => array(
        'prestarock/simplecarrier' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'a0fbac5b22ca489527aae14bd20032d899edc193',
            'dev_requirement' => false,
        ),
    ),
);
