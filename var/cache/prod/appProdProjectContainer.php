<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerXuuclgx\appProdProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerXuuclgx/appProdProjectContainer.php') {
    touch(__DIR__.'/ContainerXuuclgx.legacy');

    return;
}

if (!\class_exists(appProdProjectContainer::class, false)) {
    \class_alias(\ContainerXuuclgx\appProdProjectContainer::class, appProdProjectContainer::class, false);
}

return new \ContainerXuuclgx\appProdProjectContainer([
    'container.build_hash' => 'Xuuclgx',
    'container.build_id' => 'f8e0dbff',
    'container.build_time' => 1624965052,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerXuuclgx');
